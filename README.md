RTL SDR FM Player
===================
Turns your Realtek RTL2832 based DVB dongle into a FM radio stereo output device. Note that it requires ALSA support and an attached output device.
This project is built using rtl-sdr [http://sdr.osmocom.org/trac/wiki/rtl-sdr]  
This project is conceptually similar to rtl-fm-streamer, found at [https://github.com/AlbrechtL/rtl_fm_streamer]. While the end result ultimately takes little from it, it was built using this as a reference and thus deserves credit. 

This project is available to you under the GPL-3 license. See License.txt for more info.  
To comply with GPL-3, a changelog has been created, as well as documentation on each source file that has been modified. Unaltered code remains the intellectual property of its creators as defined by the GPL versions 2 and/or 3.

Description
-----------
RTL SDR FM Player is a small tool to take the audio directly off the device and play it on your speakers. it uses ALSA, so it is limited to \*nix platforms

The DVB-T dongle has to be based on the Realtek RTL2832U.
See [http://sdr.osmocom.org/trac/wiki/rtl-sdr](http://sdr.osmocom.org/trac/wiki/rtl-sdr) for more RTL SDR details.

## Installing

This project requires an RTL dongle and rtl-sdr to work. it also requires ALSA sound development tools (libasound).  
For instructions on how to get rtl-sdr, please follow the instructions rtl-sdr [here](http://sdr.osmocom.org/trac/wiki/rtl-sdr).  

ALSA Sound varies depending on your distro. For Debian/Raspbian:
```sh
$ sudo apt-get install libasound2-dev
```
Then simply run make, as a Makefile has been included.

Usage
------------
Currently, it functions nearly identical to rtl_fm, however, it does not need to be piped to another program - sound output is done internally to the default ALSA device.
For more information, check [here](http://kmkeen.com/rtl-demod-guide/), Or the man page for rtl_fm.  
Please note that any functions relating to saving this to a file have been removed. 

## Modification
-------------
This project can be run as a standalone, simple application. It will allow you to pick and play one station, as if it was simply rtl_fm piped to play. However, the power lies in the fact that it can be integrated into other apps.  
Defining a flag in the rtl_fm_player header file, INTEGRATED_INTO_OTHER, unlocks this behavior. Be warned that I am not obligated to help you if you choose to modify the code and things break, however, you can still ask; I may help if i feel generous.  

The following functions can then be utilized:

Function\_[variables]               | Effect  
:---------------------------------- |:----------------------------------------------------  
int rtl_fm_init(int, char**)        | Initializes the radio for use. functions identically to the main method of rtl_fm_player that would be called if INTEGRATED_INTO_OTHER was not defined. Returns the pointer to the instance of the rtl object, or negative if it failed to initialize.  
void interact\_setup(int, int, int) | Sets the data for changing frequencies: minimum, maximum, and step  
void interact\_tune(int)            | Tunes to the given frequency  
void interact\_chan(int)            | Tunes to the given channel. Functionally, parses the channel given and calls interact_tune.  
void interact\_seek(bool)           | If the boolean passed in is true, seeks up. otherwise seeks down  
void interact\_step(bool)           | If the boolean passed in is true, changes the station by the amount set in interact_setup up. if false, the same, but down.  
void interact\_scan()               | Seeks up until a station is found. after a period of time, it seeks up again. This is done asynchronously, and will continue until told to stop or another interact_ function is called.  
void interact\_clear()              | Stops a scan or a seek.  
int interact\_get_freq()            | Returns the current frequency.  
void interact\_mute(bool)           | If the boolean passed in is true, mutes the output if not already muted. if false, unmutes the volume unless already not muted.  
void interact\_exit                 | tells the radio to stop, and allows all threads to run to completion. does not end the program.  
int rtl_fm_exit(int)                | Acts the same as interact_exit, however, it also cleans up the threads. returns the value passed in or -1 if rtl_sdr failed.  

### VOLUME FUNCTIONS:  
Note that these functions simply manipulate the output by multiplying the data by a set amount based on volume, and as such may not provide desired results (audio theory is slightly out of my skillset). Nonetheless, they are available.  

Function\_[variables]               | Effect  
:---------------------------------- |:----------------------------------------------------  
byte interact\_get\_volume()        | Gets the internal volume. Value is stored as a byte, ranging from 0 to 200. This is a percent, ranging from no sound (0) to twice as loud (200)
void interact\_set\_volume(byte)    | Sets the volume internally. Volume is stored as a byte from 0 to 200. This is implemented independently of system sound.  
void interact\_inc\_volume()        | Increases the internal volume by 1. Calling this will unmute the station if muted. If at 200, does nothing  
void interact_dec_volume()          | Decreases the internal volume by 1. Has no effect on mute. If at 0, does nothing.  