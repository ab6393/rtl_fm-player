BIN := carpc-controller
CC := gcc
LD := gcc

CPP_FLAGS := -Wall -Wextra -Wno-unused -Wno-unused-parameter -Wno-unknown-pragmas -pedantic
C_FLAGS := -std=gnu99
C_FILES := $(wildcard src/*.c)
OBJ_FILES := $(addprefix obj/,$(notdir $(C_FILES:.c=.o))) 

INC_DIRS := -Iinclude -I/usr/local/include -I/usr/include/libusb-1.0

COMPILERTL := -lrtl-sdr
LINKRTL := -lrtlsdr

LIB_DIRS := -L/usr/local/lib
LIBS := -pthread -lasound 


LD_FLAGS := $(LIB_DIRS) $(LIBS)
CC_FLAGS := $(INC_DIRS)

$(BIN): $(OBJ_FILES)
	$(CPP) $(LD_FLAGS) $(LINKRTL) -o $@ $^

obj/%.o: src/%.c
	mkdir -p obj
	$(CC) $(C_FLAGS) $(CC_FLAGS) $(LD_FLAGS) $(COMPILERTL) -c -o $@ $<

clean:
	rm -rf obj/* rtl_fm_player
