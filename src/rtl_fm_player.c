#pragma region

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <math.h>
#include <pthread.h>

#include "convenience.h"

#include <time.h>
#include "rtl_fm_player.h"

#pragma endregion FM Includes and Defines

#pragma region
#include <alsa/asoundlib.h>
#include <alsa/pcm.h>
#include <stdio.h>
#pragma endregion ALSA Includes and Defines

#pragma region 
static volatile bool do_exit = false;
static int lcm_post[17] = { 1,1,1,3,1,5,3,7,1,9,5,11,3,13,7,15,1 };
static int ACTUAL_BUF_LENGTH;

static int *atan_lut = NULL;
static int atan_lut_size = 131072; /* 512 KB */
static int atan_lut_coef = 8;

static int cic_9_tables[][10] = {
	{ 0, },
{ 9, -156,  -97, 2798, -15489, 61019, -15489, 2798,  -97, -156 },
{ 9, -128, -568, 5593, -24125, 74126, -24125, 5593, -568, -128 },
{ 9, -129, -639, 6187, -26281, 77511, -26281, 6187, -639, -129 },
{ 9, -122, -612, 6082, -26353, 77818, -26353, 6082, -612, -122 },
{ 9, -120, -602, 6015, -26269, 77757, -26269, 6015, -602, -120 },
{ 9, -120, -582, 5951, -26128, 77542, -26128, 5951, -582, -120 },
{ 9, -119, -580, 5931, -26094, 77505, -26094, 5931, -580, -119 },
{ 9, -119, -578, 5921, -26077, 77484, -26077, 5921, -578, -119 },
{ 9, -119, -577, 5917, -26067, 77473, -26067, 5917, -577, -119 },
{ 9, -199, -362, 5303, -25505, 77489, -25505, 5303, -362, -199 },
};

// multiple of these, eventually
struct dongle_state dongle;
struct demod_state demod;
struct output_state output;
struct controller_state controller;
struct interact_state interact;
struct player_state player;
fifo_t fifo;
#pragma endregion Globals

#pragma region
void fifo_init(fifo_t * f, unsigned int size)
{
	f->head = 0;
	f->tail = 0;
	f->size = size;
	f->bytesAvailable = 0;
	f->buf = calloc(size, sizeof(char));
}

int fifo_read(fifo_t * f, void * buf, unsigned int nbytes)
{
	unsigned int i;
	char * p;
	p = buf;
	for (i = 0; i < nbytes; i++)
	{
		if (f->tail != f->head)
		{ //see if any data is available
			*p++ = f->buf[f->tail];  //grab a byte from the buffer
			f->tail++;  //increment the tail
			if (f->tail == f->size)
			{  //check for wrap-around
				f->tail = 0;
			}
		}
		else
		{
			f->bytesAvailable = 0;
			return i; //number of bytes read
		}
	}
	f->bytesAvailable -= nbytes;
	return nbytes;
}

int fifo_write(fifo_t * f, const void * buf, unsigned int nbytes)
{
	unsigned int i;
	const char * p;
	p = buf;
	for (i = 0; i < nbytes; i++)
	{
		//first check to see if there is space in the buffer
		if ((f->head + 1 == f->tail) || ((f->head + 1 == f->size) && (f->tail == 0)))
		{
			f->bytesAvailable += i;
			return i; //no more room
		}
		else
		{
			f->buf[f->head] = *p++;
			f->head++;  //increment the head
			if (f->head == f->size)
			{  //check for wrap-around
				f->head = 0;
			}
		}
	}
	f->bytesAvailable += nbytes;
	return nbytes;
}

bool fifo_can_read_bytes(fifo_t * f, unsigned int nbytes)
{
	return f->bytesAvailable >= nbytes;
}

void fifo_cleanup(fifo_t * f)
{
	free(f->buf);
}


void dongle_init(struct dongle_state *dg)
{
	dg->rate = DEFAULT_SAMPLE_RATE;
	dg->gain = AUTO_GAIN; // tenths of a dB
	dg->muteBytesAmount = 0;
	dg->muteAll = false;
	dg->direct_sampling = false;
	dg->offset_tuning = false;
	dg->demod_target = &demod;
}


void demod_init(struct demod_state *dm)
{
	dm->rate_in = DEFAULT_SAMPLE_RATE;
	dm->rate_out = DEFAULT_SAMPLE_RATE;
	dm->squelch_level = 0;
	dm->conseq_squelch = 10;
	dm->terminate_on_squelch = false;
	dm->squelch_exit = false;
	dm->squelch_hits = 11;
	dm->downsample_passes = 0;
	dm->comp_fir_size = 0;
	dm->prev_index = 0;
	dm->post_downsample = 1;  // once this works, default = 4
	dm->custom_atan = 0;
	dm->deemph = false;
	dm->rate_out2 = -1;  // flag for disabled
	dm->mode_demod = &fm_demod;
	dm->pre_j = dm->pre_r = dm->now_r = dm->now_j = 0;
	dm->prev_lpr_index = 0;
	dm->deemph_a = 0;
	dm->now_lpr = 0;
	dm->dc_block = false;
	dm->dc_avg = 0;
	pthread_rwlock_init(&dm->rw, NULL);
	pthread_cond_init(&dm->ready, NULL);
	pthread_mutex_init(&dm->ready_m, NULL);
	dm->output_target = &output;
}

void demod_cleanup(struct demod_state *d)
{
	pthread_rwlock_destroy(&d->rw);
	pthread_cond_destroy(&d->ready);
	pthread_mutex_destroy(&d->ready_m);
}

void output_init(struct output_state *o)
{
	o->rate = DEFAULT_SAMPLE_RATE;
	o->volume = 0;
	pthread_rwlock_init(&o->rw, NULL);
	pthread_cond_init(&o->ready, NULL);
	pthread_mutex_init(&o->ready_m, NULL);
	o->player_target = &player;
}

void output_cleanup(struct output_state *o)
{
	pthread_rwlock_destroy(&o->rw);
	pthread_cond_destroy(&o->ready);
	pthread_mutex_destroy(&o->ready_m);
}


void controller_init(struct controller_state *c)
{
	c->freqs[0] = 100000000;
	c->freq_len = 0;
	c->freq_now = 0;
	c->edge = 0;
	c->wb_mode = false;
	c->status = NONE;
	pthread_cond_init(&c->interact, NULL);
	pthread_mutex_init(&c->interact_m, NULL);
}

void controller_cleanup(struct controller_state *c)
{
	pthread_cond_destroy(&c->interact);
	pthread_mutex_destroy(&c->interact_m);
}


void interact_init(struct interact_state *i)
{
	pthread_rwlock_init(&i->status_lock, NULL);

	i->freq_min = MIN_FREQ_DEFAULT;
	i->freq_max = MAX_FREQ_DEFAULT;
	i->bandwidth = BANDWIDTH_EU;
	i->status = NONE;

	i->seekUp = true;
	i->freq = MIN_FREQ_DEFAULT;
	i->muted = false;
	i->volume = 0;
	i->scan_delay = DEFAULT_SCAN_DELAY;
}

void interact_cleanup(struct interact_state * i)
{
	pthread_rwlock_destroy(&i->status_lock);
}


void player_init(struct player_state * p)
{
	p->channels = CHAN_MONO;
	p->setup = false;
	p->rate = DEFAULT_SAMPLE_RATE;
	p->period_size = PERIOD_SIZE;

	pthread_rwlock_init(&p->rw, NULL);
	p->pcm_callback = NULL;
	p->callbackRunning = false;
	p->fifo = &fifo;
}

void player_setup(struct player_state * p)
{
	snd_pcm_hw_params_t *params;
	snd_pcm_sw_params_t *sw_params;

	/* Open the PCM device in playback mode */
	if ((p->pcm_error = snd_pcm_open(&(p->pcm_handle), PCM_DEVICE, SND_PCM_STREAM_PLAYBACK, 0)) < 0)
	{
		printf("ERROR: Can't open \"%s\" PCM device. %s\n", PCM_DEVICE, snd_strerror(p->pcm_error));
	}

	/* Allocate parameters object and fill it with default values*/
	//alloca puts params on the stack, no need to free it.
	snd_pcm_hw_params_alloca(&params);
	if ((p->pcm_error = snd_pcm_hw_params_any(p->pcm_handle, params)) < 0)
	{
		printf("Broken configuration for playback: no configurations available: %s\n", snd_strerror(p->pcm_error));
	}

	/* Update relevant parameters to new values */
	//interleaved
	if ((p->pcm_error = snd_pcm_hw_params_set_access(p->pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
	{
		printf("ERROR: Can't set interleaved mode. %s\n", snd_strerror(p->pcm_error));
	}
	//16 bit little endian raw data
	if ((p->pcm_error = snd_pcm_hw_params_set_format(p->pcm_handle, params, SND_PCM_FORMAT_S16_LE)) < 0)
	{
		printf("ERROR: Can't set format. %s\n", snd_strerror(p->pcm_error));
	}

	//channels (mono:1, stereo:2)
	if ((p->pcm_error = snd_pcm_hw_params_set_channels(p->pcm_handle, params, p->channels)) < 0)
	{
		printf("ERROR: Can't set channels number. %s\n", snd_strerror(p->pcm_error));
	}
	//sample rate
	if ((p->pcm_error = snd_pcm_hw_params_set_rate_near(p->pcm_handle, params, &(p->rate), 0)) < 0)
	{
		printf("ERROR: Can't set rate. %s\n", snd_strerror(p->pcm_error));
	}

	//period size
	if ((p->pcm_error = snd_pcm_hw_params_set_period_size_near(p->pcm_handle, params, &(p->period_size), 0)) < 0)
	{
		printf("ERROR: Can't set period size. %s\n", snd_strerror(p->pcm_error));
	}

	/* Write parameters to pcm_handler */
	if ((p->pcm_error = snd_pcm_hw_params(p->pcm_handle, params)) < 0)
	{
		printf("ERROR: Can't set harware parameters. %s\n", snd_strerror(p->pcm_error));
	}

	snd_pcm_sw_params_alloca(&sw_params);

	if ((p->pcm_error = snd_pcm_sw_params_current(p->pcm_handle, sw_params)) < 0)
	{
		printf("Unable to determine current swparams for playback: %s\n", snd_strerror(p->pcm_error));
	}

	//as soon as something can be played, play it.
	if ((p->pcm_error = snd_pcm_sw_params_set_start_threshold(p->pcm_handle, sw_params, MIN_DATA)) < 0)
	{
		printf("Unable to set start threshold mode for playback: %s\n", snd_strerror(p->pcm_error));
	}
	//let us know as soon as we need a period so we can fill it.
	if ((p->pcm_error = snd_pcm_sw_params_set_avail_min(p->pcm_handle, sw_params, p->period_size)) < 0)
	{
		printf("Unable to set avail min for playback: %s\n", snd_strerror(p->pcm_error));
	}

	if ((p->pcm_error = snd_pcm_sw_params(p->pcm_handle, sw_params)) < 0)
	{
		printf("Unable to set sw params for playback: %s\n", snd_strerror(p->pcm_error));
	}

	fifo_init(p->fifo, MAXIMUM_BUF_LENGTH * 8);
	p->setup = true;
}

void player_cleanup(struct player_state * p)
{
	pthread_rwlock_destroy(&p->rw);
	if (p->setup)
	{
		fifo_cleanup(p->fifo);
		p->setup = false;
	}
}

#pragma endregion Structs

#pragma region
static void optimal_settings(int freq, int rate)
{
	// giant ball of hacks
	// seems unable to do a single pass, 2:1
	int capture_freq, capture_rate;
	struct dongle_state *d = &dongle;
	struct demod_state *dm = &demod;
	struct controller_state *cs = &controller;
	dm->downsample = (1000000 / dm->rate_in) + 1;
	if (dm->downsample_passes)
	{
		dm->downsample_passes = (int) log2(dm->downsample) + 1;
		dm->downsample = 1 << dm->downsample_passes;
	}
	capture_freq = freq;
	capture_rate = dm->downsample * dm->rate_in;
	if (!d->offset_tuning)
	{
		capture_freq = freq + capture_rate / 4;
	}
	capture_freq += cs->edge * dm->rate_in / 2;
	dm->output_scale = (1 << 15) / (128 * dm->downsample);
	if (dm->output_scale < 1)
	{
		dm->output_scale = 1;
	}
	if (dm->mode_demod == &fm_demod)
	{
		dm->output_scale = 1;
	}
	d->freq = (uint32_t) capture_freq;
	d->rate = (uint32_t) capture_rate;
}

static void setup_frequency(struct controller_state *s)
{
	int i;

	if (s->wb_mode)
	{
		for (i = 0; i < s->freq_len; i++)
		{
			s->freqs[i] += 16000;
		}
	}

	/* set up primary channel */
	optimal_settings(s->freqs[0], demod.rate_in);
	if (dongle.direct_sampling)
	{
		verbose_direct_sampling(dongle.dev, 1);
	}
	if (dongle.offset_tuning)
	{
		verbose_offset_tuning(dongle.dev);
	}
	verbose_set_frequency(dongle.dev, dongle.freq);
}

static void seek(bool up)
{
	struct interact_state *i = &interact;
	struct controller_state *c = &controller;
	int curr_freq = c->freqs[c->freq_now];
	unsigned int freq = up ? i->bandwidth : -i->bandwidth;
	freq += curr_freq;
	int x;
	c->freq_len = 0;

	if (up)
	{
		for (freq; freq <= i->freq_max; freq += i->bandwidth)
		{
			c->freqs[c->freq_len++] = freq;
		}
		for (freq = i->freq_min; freq < (unsigned int)c->freq_now; freq += i->bandwidth)
		{
			c->freqs[c->freq_len++] = freq;
		}
	}
	else
	{
		for (freq; freq >= i->freq_min; freq -= i->bandwidth)
		{
			c->freqs[c->freq_len++] = freq;
		}
		for (freq = i->freq_max; freq >(unsigned int) c->freq_now; freq -= i->bandwidth)
		{
			c->freqs[c->freq_len++] = freq;
		}
	}
	c->freq_now = 0;
	setup_frequency(c);
}

static void tune()
{
	struct interact_state *i = &interact;
	struct controller_state *c = &controller;
	c->freq_len = 1;
	c->freq_now = 0;
	c->freqs[c->freq_now] = i->freq;
	setup_frequency(c);
}

static void scan()
{
	struct interact_state *i = &interact;
	struct controller_state *c = &controller;
	while (i->status == SCANNING && !do_exit)
	{
		seek(true);
		usleep(i->scan_delay);
	}
}

static bool interacting(struct demod_state * d, struct controller_state * s, struct interact_state *i)
{
	bool retVal = false;
	if (d->squelch_level != 0 && d->squelch_hits > d->conseq_squelch)
	{
		retVal = true;
		if (d->terminate_on_squelch)
		{
			do_exit = true;
			d->squelch_exit = true;
			return true;
		}
	}
	else if (i->status == SCANNING && s->status == STABLE && difftime(time(&i->end), i->start) > 2) 
	{
		retVal = true;
		s->status = SCANNING;
	}
	else if (i->status != s->status)
	{
		retVal = true;
	}

	if (retVal)
	{
		safe_cond_signal(&controller.interact, &controller.interact_m);
	}
	return retVal;

}

#pragma endregion Private Threading Helpers

#pragma region
static void *dongle_thread_fn(void *arg)
{
	struct dongle_state *s = arg;
	rtlsdr_read_async(s->dev, rtlsdr_callback, s, 0, s->buf_len);
	return 0;
}

static void *demod_thread_fn(void *arg)
{
	struct demod_state *d = arg;
	struct output_state *o = d->output_target;

	while (!do_exit)
	{
		safe_cond_wait(&d->ready, &d->ready_m);
		pthread_rwlock_wrlock(&d->rw);
		full_demod(d);
		pthread_rwlock_unlock(&d->rw);
		//should never be hit unless something goes terribly wrong. 
		if (d->exit_flag != 0)
		{
			do_exit = true;
		}
		if (interacting(d, &controller, &interact))
		{
			if (d->squelch_exit) 
			{
				return 0;
			}
			continue;
		}

		pthread_rwlock_wrlock(&o->rw);
		memcpy(o->result, d->result, 2 * d->result_len);
		o->result_len = d->result_len;
		pthread_rwlock_unlock(&o->rw);
		safe_cond_signal(&o->ready, &o->ready_m);
	}
	return 0;
}

static void *output_thread_fn(void *arg)
{
	//prevents deadlock, otherwise redundant
	int16_t data[MAXIMUM_BUF_LENGTH];
	int debug_bytes_written;
	int data_len = 0;
	struct output_state *o = arg;
	struct player_state *p = o->player_target;
	//set in init, but here for reference.
	while (!do_exit)
	{
		// use timedwait and pad out under runs
		safe_cond_wait(&o->ready, &o->ready_m);
		pthread_rwlock_rdlock(&o->rw);
		memcpy(data, o->result, o->result_len * 2);
		data_len = o->result_len;
		pthread_rwlock_unlock(&o->rw);

		pthread_rwlock_wrlock(&p->rw);
		debug_bytes_written = fifo_write(p->fifo, data, data_len * 2);
		pthread_rwlock_unlock(&p->rw);

		if (debug_bytes_written < data_len * 2)
		{
			printf("ERROR: buffer overran. discarding %i bytes\n", data_len * 2 - debug_bytes_written);
		}

	}

	return 0;
}

static void *player_thread_fn(void * arg)
{
	struct player_state *p = arg;
	int bytesRead = 0;
	int16_t data[PERIOD_SIZE];
	snd_pcm_writei(p->pcm_handle, data, bytesRead * 2);
	while (!fifo_can_read_bytes(p->fifo, MIN_DATA))
	{
		if (do_exit)
		{
			return 0;
		}
		usleep(100000);
	}

	pthread_rwlock_rdlock(&p->rw);
	bytesRead = fifo_read(p->fifo, data, p->period_size * 2);
	pthread_rwlock_unlock(&p->rw);
	snd_pcm_writei(p->pcm_handle, data, bytesRead * 2);
	snd_pcm_start(p->pcm_handle);

	snd_async_add_pcm_handler(&(p->pcm_callback), p->pcm_handle, pcm_callback_fn, p);
	return 0;
}

static void *controller_thread_fn(void *arg)
{
	struct controller_state *s = arg;
	struct interact_state *i = &interact;

	setup_frequency(s);

	fprintf(stderr, "Oversampling input by: %ix.\n", demod.downsample);
	fprintf(stderr, "Oversampling output by: %ix.\n", demod.post_downsample);
	fprintf(stderr, "Buffer size: %0.2fms\n",
		1000 * 0.5 * (float) ACTUAL_BUF_LENGTH / (float) dongle.rate);

	/* Set the sample rate */
	verbose_set_sample_rate(dongle.dev, dongle.rate);
	fprintf(stderr, "Output at %u Hz.\n", demod.rate_in / demod.post_downsample);

	while (!do_exit)
	{
		//block until the demod thread tells us we need to do something.
		safe_cond_wait(&s->interact, &s->interact_m);
		if (i->status != s->status)
		{
			switch (i->status)
			{
			case SCANNING:
				i->seekUp = true;
			case SEEKING:
				seek(i->seekUp);
				break;
			case TUNING:
				tune();
				i->status = NONE;
				safe_cond_signal(&interact.scan, &interact.scan_m);
			break;
				case VOLUMECHANGED:
				dongle.muteAll = i->muted;
				output.volume = i->volume;
				i->status = NONE;
				safe_cond_signal(&interact.scan, &interact.scan_m);
				break;
			case CLEAR:
			case STABLE:
				i->status = NONE;
				safe_cond_signal(&interact.scan, &interact.scan_m);
			case NONE:
			default:
				break;
			}
			s->status = i->status;
		}
		else if (s->status == SCANNING || s->status == SEEKING)
		{
			s->freq_now = (s->freq_now + 1) % s->freq_len;
			optimal_settings(s->freqs[s->freq_now], demod.rate_in);
			rtlsdr_set_center_freq(dongle.dev, dongle.freq);
			dongle.muteBytesAmount = BUFFER_DUMP;
		}
	}
	return 0;
}
#pragma endregion Threading Functions

#pragma region
static void rtlsdr_callback(unsigned char *buf, uint32_t len, void *ctx)
{
	unsigned int i;

	struct dongle_state *s = ctx;
	struct demod_state *d = s->demod_target;
	int muteAmount = 0;

	if (do_exit)
	{
		return;
	}
	if (ctx == NULL)
	{
		return;
	}
	if (s->muteBytesAmount > 0 || s->muteAll)
	{
		muteAmount = s->muteAll ? len : s->muteBytesAmount;
		memset(buf, 127, muteAmount);
		s->muteBytesAmount = 0;
	}
	if (!s->offset_tuning)
	{
		rotate_90(buf, len);
	}
	for (i = 0; i < len; i++)
	{
		s->buf16[i] = (int16_t) buf[i] - 127;
	}
	pthread_rwlock_wrlock(&d->rw);
	memcpy(d->lowpassed, s->buf16, 2 * len);
	d->lp_len = len;
	pthread_rwlock_unlock(&d->rw);
	safe_cond_signal(&d->ready, &d->ready_m);
}

static void pcm_callback_fn(snd_async_handler_t *pcm_callback)
{

	int bytesRead = 0;
	struct player_state *p = snd_async_handler_get_callback_private(pcm_callback);
	int16_t data[MAXIMUM_BUF_LENGTH];
	snd_pcm_sframes_t avail;
	long periodSize = 0;

	if (do_exit)
	{
		return;
	}
	if (p->callbackRunning)
	{
		return;
	}
	p->callbackRunning = true;
	if (p->pcm_handle != snd_async_handler_get_pcm(pcm_callback))
	{
		printf("Unexpected error.\n");
	}
	avail = snd_pcm_avail(p->pcm_handle);

	periodSize = p->period_size > __LONG_MAX__ ? __LONG_MAX__ : (long) p->period_size;
	while (avail >= periodSize)
	{
		if (do_exit)
		{
			p->callbackRunning = false;
			return;
		}
		pthread_rwlock_rdlock(&p->rw);
		bytesRead = fifo_read(p->fifo, data, avail * 2);
		pthread_rwlock_unlock(&p->rw);
		if (bytesRead == 0)
		{
			printf("DEBUG: no bytes available\n");
			memset(data, 0, avail * 2);
			bytesRead = avail * 2;
		}

		if ((p->pcm_error = snd_pcm_writei(p->pcm_handle, data, bytesRead / 2)) == -EPIPE)
		{
			printf("Broken Pipe. preparing. Error: %s\n", snd_strerror(p->pcm_error));
			snd_pcm_prepare(p->pcm_handle);
		}
		else if (p->pcm_error < 0)
		{
			printf("ERROR. Can't write to PCM device. %s\n", snd_strerror(p->pcm_error));
		}
		avail = snd_pcm_avail(p->pcm_handle);
	}
	p->callbackRunning = false;
}
#pragma endregion Callbacks

#pragma region 

void interact_setup(int min, int max, int step) 
{
	interact.freq_min = min;
	interact.freq_max = max;
	interact.bandwidth = step;
}

void interact_tune(int freq)
{
	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.freq = freq;
	interact.status = TUNING;
}

void interact_chan(int chan) 
{
	chan += (int) (interact.freq_min / interact.bandwidth);
	interact_tune(chan);
}

void interact_seek(bool up)
{
	if (interact.status == SCANNING)
	{
		interact.status = CLEAR;
	}
	else if (interact.status == SEEKING) 
	{
		return;
	}

	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.status = SEEKING;

}

void interact_seek_with_callback(bool up, void(*RadioFrequencyUpdate)(int, bool))
{
	if (interact.status == SCANNING)
	{
		interact.status = CLEAR;
	}
	else if (interact.status == SEEKING)
	{
		return;
	}

	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.status = SEEKING;
	while (interact.status == SEEKING && !do_exit) 
	{
		(*RadioFrequencyUpdate)(interact_get_freq(), false);
		usleep(10000); //delay(10). 
	}
}

void interact_step(bool up)
{
	
	if (interact.status == SEEKING || interact.status == SCANNING)
	{
		interact.status = CLEAR;
	}
	if(interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	int offset = up ? interact.bandwidth : -interact.bandwidth;
	interact.freq = controller.freqs[controller.freq_now] + offset;
	interact.status = TUNING;

}

int interact_get_freq()
{
	return controller.freqs[controller.freq_now];
}

void interact_scan()
{
	if (interact.status == SEEKING)
	{
		interact.status = CLEAR;
	}
	else if (interact.status == SCANNING) 
	{
		return;
	}
	if (interact.status != NONE) 
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.status = SCANNING;
}

void interact_clear()
{
	if (interact.status == SCANNING || interact.status == SEEKING)
	{
		interact.status = CLEAR;
	}
}

void interact_inc_volume()
{
	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.muted = false;
	interact.volume++;
	interact.status = VOLUMECHANGED;
}

void interact_dec_volume()
{
	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.volume--;
	interact.status = VOLUMECHANGED;
}

void interact_set_volume(int volume)
{
	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	interact.muted = volume > interact.volume ? false : interact.muted;
	interact.volume = volume;
	interact.status = VOLUMECHANGED;
}

int interact_get_volume()
{
	if (interact.status != NONE)
	{
		safe_cond_wait(&interact.scan, &interact.scan_m);
	}
	return interact.muted ? 0 : interact.volume;
}

int interact_get_rds(char * str, int strlen)
{
	return 0;
}

void interact_set_mute(bool isMuted)
{
	if (interact.muted != isMuted)
	{
		if (interact.status != NONE)
		{
			safe_cond_wait(&interact.scan, &interact.scan_m);
		}
		interact.muted = isMuted;
		interact.status = VOLUMECHANGED;
	}
}

void interact_exit() 
{
	do_exit = true;
}
#pragma endregion interactive Functions

#pragma region

static void sighandler(int signum)
{
	fprintf(stderr, "Signal caught, exiting!\n");
	do_exit = true;
	rtlsdr_cancel_async(dongle.dev);
	if (player.pcm_callback != NULL)
	{
		snd_async_del_handler(player.pcm_callback);
		player.pcm_callback = NULL;
	}

}

void rotate_90(unsigned char *buf, uint32_t len)
/* 90 rotation is 1+0j, 0+1j, -1+0j, 0-1j
or [0, 1, -3, 2, -4, -5, 7, -6] */
{
	uint32_t i;
	unsigned char tmp;
	for (i = 0; i < len; i += 8)
	{
		/* uint8_t negation = 255 - x */
		tmp = 255 - buf[i + 3];
		buf[i + 3] = buf[i + 2];
		buf[i + 2] = tmp;

		buf[i + 4] = 255 - buf[i + 4];
		buf[i + 5] = 255 - buf[i + 5];

		tmp = 255 - buf[i + 6];
		buf[i + 6] = buf[i + 7];
		buf[i + 7] = tmp;
	}
}

void low_pass(struct demod_state *d)
/* simple square window FIR */
{
	int i = 0, i2 = 0;
	while (i < d->lp_len)
	{
		d->now_r += d->lowpassed[i];
		d->now_j += d->lowpassed[i + 1];
		i += 2;
		d->prev_index++;
		if (d->prev_index < d->downsample)
		{
			continue;
		}
		d->lowpassed[i2] = d->now_r; // * d->output_scale;
		d->lowpassed[i2 + 1] = d->now_j; // * d->output_scale;
		d->prev_index = 0;
		d->now_r = 0;
		d->now_j = 0;
		i2 += 2;
	}
	d->lp_len = i2;
}

int low_pass_simple(int16_t *signal2, int len, int step)
// no wrap around, length must be multiple of step
{
	int i, i2, sum;
	for (i = 0; i < len; i += step)
	{
		sum = 0;
		for (i2 = 0; i2 < step; i2++)
		{
			sum += (int) signal2[i + i2];
		}
		//signal2[i/step] = (int16_t)(sum / step);
		signal2[i / step] = (int16_t) (sum);
	}
	signal2[i / step + 1] = signal2[i / step];
	return len / step;
}

void low_pass_real(struct demod_state *s)
/* simple square window FIR */
// add support for upsampling?
{
	int i = 0, i2 = 0;
	int fast = (int) s->rate_out;
	int slow = s->rate_out2;
	while (i < s->result_len)
	{
		s->now_lpr += s->result[i];
		i++;
		s->prev_lpr_index += slow;
		if (s->prev_lpr_index < fast)
		{
			continue;
		}
		s->result[i2] = (int16_t) (s->now_lpr / (fast / slow));
		s->prev_lpr_index -= fast;
		s->now_lpr = 0;
		i2 += 1;
	}
	s->result_len = i2;
}

void fifth_order(int16_t *data, int length, int16_t *hist)
/* for half of interleaved data */
{
	int i;
	int16_t a, b, c, d, e, f;
	a = hist[1];
	b = hist[2];
	c = hist[3];
	d = hist[4];
	e = hist[5];
	f = data[0];
	/* a downsample should improve resolution, so don't fully shift */
	data[0] = (a + (b + e) * 5 + (c + d) * 10 + f) >> 4;
	for (i = 4; i < length; i += 4)
	{
		a = c;
		b = d;
		c = e;
		d = f;
		e = data[i - 2];
		f = data[i];
		data[i / 2] = (a + (b + e) * 5 + (c + d) * 10 + f) >> 4;
	}
	/* archive */
	hist[0] = a;
	hist[1] = b;
	hist[2] = c;
	hist[3] = d;
	hist[4] = e;
	hist[5] = f;
}

void generic_fir(int16_t *data, int length, int *fir, int16_t *hist)
/* Okay, not at all generic.  Assumes length 9, fix that eventually. */
{
	int d, temp, sum;
	for (d = 0; d < length; d += 2)
	{
		temp = data[d];
		sum = 0;
		sum += (hist[0] + hist[8]) * fir[1];
		sum += (hist[1] + hist[7]) * fir[2];
		sum += (hist[2] + hist[6]) * fir[3];
		sum += (hist[3] + hist[5]) * fir[4];
		sum += hist[4] * fir[5];
		data[d] = sum >> 15;
		hist[0] = hist[1];
		hist[1] = hist[2];
		hist[2] = hist[3];
		hist[3] = hist[4];
		hist[4] = hist[5];
		hist[5] = hist[6];
		hist[6] = hist[7];
		hist[7] = hist[8];
		hist[8] = temp;
	}
}

/* define our own complex math ops
because ARMv5 has no hardware float */

void multiply(int ar, int aj, int br, int bj, int *cr, int *cj)
{
	*cr = ar * br - aj * bj;
	*cj = aj * br + ar * bj;
}

int polar_discriminant(int ar, int aj, int br, int bj)
{
	int cr, cj;
	double angle;
	multiply(ar, aj, br, -bj, &cr, &cj);
	angle = atan2((double) cj, (double) cr);
	return (int) (angle / 3.14159 * (1 << 14));
}

int fast_atan2(int y, int x)
/* pre scaled for int16 */
{
	int yabs, angle;
	int pi4 = (1 << 12), pi34 = 3 * (1 << 12);  // note pi = 1<<14
	if (x == 0 && y == 0)
	{
		return 0;
	}
	yabs = y;
	if (yabs < 0)
	{
		yabs = -yabs;
	}
	if (x >= 0)
	{
		angle = pi4 - pi4 * (x - yabs) / (x + yabs);
	}
	else
	{
		angle = pi34 - pi4 * (x + yabs) / (yabs - x);
	}
	if (y < 0)
	{
		return -angle;
	}
	return angle;
}

int polar_disc_fast(int ar, int aj, int br, int bj)
{
	int cr, cj;
	multiply(ar, aj, br, -bj, &cr, &cj);
	return fast_atan2(cj, cr);
}

int atan_lut_init(void)
{
	int i = 0;

	atan_lut = malloc(atan_lut_size * sizeof(int));

	for (i = 0; i < atan_lut_size; i++)
	{
		atan_lut[i] = (int) (atan((double) i / (1 << atan_lut_coef)) / 3.14159 * (1 << 14));
	}

	return 0;
}

int polar_disc_lut(int ar, int aj, int br, int bj)
{
	int cr, cj, x, x_abs;

	multiply(ar, aj, br, -bj, &cr, &cj);

	/* special cases */
	if (cr == 0 || cj == 0)
	{
		if (cr == 0 && cj == 0)
		{
			return 0;
		}
		if (cr == 0 && cj > 0)
		{
			return 1 << 13;
		}
		if (cr == 0 && cj < 0)
		{
			return -(1 << 13);
		}
		if (cj == 0 && cr > 0)
		{
			return 0;
		}
		if (cj == 0 && cr < 0)
		{
			return 1 << 14;
		}
	}

	/* real range -32768 - 32768 use 64x range -> absolute maximum: 2097152 */
	x = (cj << atan_lut_coef) / cr;
	x_abs = abs(x);

	if (x_abs >= atan_lut_size)
	{
		/* we can use linear range, but it is not necessary */
		return (cj > 0) ? 1 << 13 : -1 << 13;
	}

	if (x > 0)
	{
		return (cj > 0) ? atan_lut[x] : atan_lut[x] - (1 << 14);
	}
	else
	{
		return (cj > 0) ? (1 << 14) - atan_lut[-x] : -atan_lut[-x];
	}

	return 0;
}

void fm_demod(struct demod_state *fm)
{
	int i, pcm;
	int16_t *lp = fm->lowpassed;
	pcm = polar_discriminant(lp[0], lp[1],
		fm->pre_r, fm->pre_j);
	fm->result[0] = (int16_t) pcm;
	for (i = 2; i < (fm->lp_len - 1); i += 2)
	{
		switch (fm->custom_atan)
		{
		case 0:
		pcm = polar_discriminant(lp[i], lp[i + 1],
			lp[i - 2], lp[i - 1]);
		break;
		case 1:
		pcm = polar_disc_fast(lp[i], lp[i + 1],
			lp[i - 2], lp[i - 1]);
		break;
		case 2:
		pcm = polar_disc_lut(lp[i], lp[i + 1],
			lp[i - 2], lp[i - 1]);
		break;
		}
		fm->result[i / 2] = (int16_t) pcm;
	}
	fm->pre_r = lp[fm->lp_len - 2];
	fm->pre_j = lp[fm->lp_len - 1];
	fm->result_len = fm->lp_len / 2;
}

void am_demod(struct demod_state *fm)
// todo, fix this extreme laziness
{
	int i, pcm;
	int16_t *lp = fm->lowpassed;
	int16_t *r = fm->result;
	for (i = 0; i < fm->lp_len; i += 2)
	{
		// hypot uses floats but won't overflow
		//r[i/2] = (int16_t)hypot(lp[i], lp[i+1]);
		pcm = lp[i] * lp[i];
		pcm += lp[i + 1] * lp[i + 1];
		r[i / 2] = (int16_t) sqrt(pcm) * fm->output_scale;
	}
	fm->result_len = fm->lp_len / 2;
	// lowpass? (3khz)  highpass?  (dc)
}

void usb_demod(struct demod_state *fm)
{
	int i, pcm;
	int16_t *lp = fm->lowpassed;
	int16_t *r = fm->result;
	for (i = 0; i < fm->lp_len; i += 2)
	{
		pcm = lp[i] + lp[i + 1];
		r[i / 2] = (int16_t) pcm * fm->output_scale;
	}
	fm->result_len = fm->lp_len / 2;
}

void lsb_demod(struct demod_state *fm)
{
	int i, pcm;
	int16_t *lp = fm->lowpassed;
	int16_t *r = fm->result;
	for (i = 0; i < fm->lp_len; i += 2)
	{
		pcm = lp[i] - lp[i + 1];
		r[i / 2] = (int16_t) pcm * fm->output_scale;
	}
	fm->result_len = fm->lp_len / 2;
}

void raw_demod(struct demod_state *fm)
{
	int i;
	for (i = 0; i < fm->lp_len; i++)
	{
		fm->result[i] = (int16_t) fm->lowpassed[i];
	}
	fm->result_len = fm->lp_len;
}

void deemph_filter(struct demod_state *fm)
{
	static int avg;  // cheating...
	int i, d;
	// de-emph IIR
	// avg = avg * (1 - alpha) + sample * alpha;
	for (i = 0; i < fm->result_len; i++)
	{
		d = fm->result[i] - avg;
		if (d > 0)
		{
			avg += (d + fm->deemph_a / 2) / fm->deemph_a;
		}
		else
		{
			avg += (d - fm->deemph_a / 2) / fm->deemph_a;
		}
		fm->result[i] = (int16_t) avg;
	}
}

void dc_block_filter(struct demod_state *fm)
{
	int i, avg;
	int64_t sum = 0;
	for (i = 0; i < fm->result_len; i++)
	{
		sum += fm->result[i];
	}
	avg = sum / fm->result_len;
	avg = (avg + fm->dc_avg * 9) / 10;
	for (i = 0; i < fm->result_len; i++)
	{
		fm->result[i] -= avg;
	}
	fm->dc_avg = avg;
}

int mad(int16_t *samples, int len, int step)
/* mean average deviation */
{
	int i = 0, sum = 0, ave = 0;
	if (len == 0)
	{
		return 0;
	}
	for (i = 0; i < len; i += step)
	{
		sum += samples[i];
	}
	ave = sum / (len * step);
	sum = 0;
	for (i = 0; i < len; i += step)
	{
		sum += abs(samples[i] - ave);
	}
	return sum / (len / step);
}

int rms(int16_t *samples, int len, int step)
/* largely lifted from rtl_power */
{
	int i;
	long p, t, s;
	double dc, err;

	p = t = 0L;
	for (i = 0; i < len; i += step)
	{
		s = (long) samples[i];
		t += s;
		p += s * s;
	}
	/* correct for dc offset in squares */
	dc = (double) (t*step) / (double) len;
	err = t * 2 * dc - dc * dc * len;

	return (int) sqrt((p - err) / len);
}

void arbitrary_upsample(int16_t *buf1, int16_t *buf2, int len1, int len2)
/* linear interpolation, len1 < len2 */
{
	int i = 1;
	int j = 0;
	int tick = 0;
	double frac;  // use integers...
	while (j < len2)
	{
		frac = (double) tick / (double) len2;
		buf2[j] = (int16_t) (buf1[i - 1] * (1 - frac) + buf1[i] * frac);
		j++;
		tick += len1;
		if (tick > len2)
		{
			tick -= len2;
			i++;
		}
		if (i >= len1)
		{
			i = len1 - 1;
			tick = len2;
		}
	}
}

void arbitrary_downsample(int16_t *buf1, int16_t *buf2, int len1, int len2)
/* fractional boxcar lowpass, len1 > len2 */
{
	int i = 1;
	int j = 0;
	int tick = 0;
	double remainder = 0;
	double frac;  // use integers...
	buf2[0] = 0;
	while (j < len2)
	{
		frac = 1.0;
		if ((tick + len2) > len1)
		{
			frac = (double) (len1 - tick) / (double) len2;
		}
		buf2[j] += (int16_t) ((double) buf1[i] * frac + remainder);
		remainder = (double) buf1[i] * (1.0 - frac);
		tick += len2;
		i++;
		if (tick > len1)
		{
			j++;
			buf2[j] = 0;
			tick -= len1;
		}
		if (i >= len1)
		{
			i = len1 - 1;
			tick = len1;
		}
	}
	for (j = 0; j < len2; j++)
	{
		buf2[j] = buf2[j] * len2 / len1;
	}
}

void arbitrary_resample(int16_t *buf1, int16_t *buf2, int len1, int len2)
/* up to you to calculate lengths and make sure it does not go OOB
* okay for buffers to overlap, if you are downsampling */
{
	if (len1 < len2)
	{
		arbitrary_upsample(buf1, buf2, len1, len2);
	}
	else
	{
		arbitrary_downsample(buf1, buf2, len1, len2);
	}
}

void full_demod(struct demod_state *d)
{
	struct interact_state *i = &interact;
	struct controller_state *s = &controller;
	int x, ds_p;
	int sr = 0;
	ds_p = d->downsample_passes;
	if (ds_p != 0)
	{
		for (x = 0; x < ds_p; x++)
		{
			fifth_order(d->lowpassed, (d->lp_len >> x), d->lp_i_hist[x]);
			fifth_order(d->lowpassed + 1, (d->lp_len >> x) - 1, d->lp_q_hist[x]);
		}
		d->lp_len = d->lp_len >> ds_p;
		/* droop compensation */
		if (d->comp_fir_size == 9 && ds_p <= CIC_TABLE_MAX)
		{
			generic_fir(d->lowpassed, d->lp_len,
				cic_9_tables[ds_p], d->droop_i_hist);
			generic_fir(d->lowpassed + 1, d->lp_len - 1,
				cic_9_tables[ds_p], d->droop_q_hist);
		}
	}
	else
	{
		low_pass(d);
	}
	/* power squelch */

	//squelching: if we are squelching, and we are possibly looking for a new channel
	//see if the RMS is less than a given constant (squelch) if so, increment a counter.
	//if the signal doesn't clear up after a set number of tests, we will look for a new
	//channel. if not, do not look for a new channel. 

	//if we are not seeking, this is ignored as an optimization by setting squelch hits to 0.
	//however, if we terminate after a set number of failures, we must not optimize this out.
	if (d->squelch_level != 0 && (i->status == SEEKING || i->status == SCANNING || d->terminate_on_squelch))
	{
		sr = rms(d->lowpassed, d->lp_len, 1);
		if (sr < d->squelch_level)
		{
			d->squelch_hits++;
			for (x = 0; x < d->lp_len; x++)
			{
				d->lowpassed[x] = 0;
			}
		}
		else
		{
			d->squelch_hits = 0;
			if (i->status == SEEKING)
			{
				i->status == CLEAR;
				s->status == STABLE;
			}
			else if (i->status == SCANNING) 
			{
				time(&i->start);
				s->status == STABLE;
			}
		}
	}
	else
	{
		d->squelch_hits = 0;
	}
	d->mode_demod(d);  /* lowpassed -> result */
	if (d->mode_demod == &raw_demod)
	{
		return;
	}
	/* todo, fm noise squelch */
	// use nicer filter here too?
	if (d->post_downsample > 1)
	{
		d->result_len = low_pass_simple(d->result, d->result_len, d->post_downsample);
	}
	if (d->deemph)
	{
		deemph_filter(d);
	}
	if (d->dc_block)
	{
		dc_block_filter(d);
	}
	if (d->rate_out2 > 0)
	{
		low_pass_real(d);
		//arbitrary_resample(d->result, d->result, d->result_len, d->result_len * d->rate_out2 / d->rate_out);
	}
}

#pragma endregion Radio Data Parsing Helpers

#pragma region 

//Private helpers
void usage(void)
{
	fprintf(stderr,
		"rtl_fm_play, a simple narrow band FM demodulator and player for RTL2832 based DVB-T receivers\n"
		"uses alsa, and therefore does not support Windows"
		"Use:\trtl_fm -f freq [-options]\n"
		"\t-f frequency_to_tune_to [Hz]\n"
		"\t    use multiple -f for scanning (requires squelch)\n"
		"\t    ranges supported, -f 118M:137M:25k\n"
		"\t[-M modulation (default: fm)]\n"
		"\t    fm, wbfm, raw, am, usb, lsb\n"
		"\t    wbfm == -M fm -s 170k -o 4 -A fast -r 32k -l 0 -E deemp\n"
		"\t    raw mode outputs 2x16 bit IQ pairs\n"
		"\t[-s sample_rate (default: 24k)]\n"
		"\t[-d device_index (default: 0)]\n"
		"\t[-T enable bias-T on GPIO PIN 0 (works for rtl-sdr.com v3 dongles)]\n"
		"\t[-g tuner_gain (default: automatic)]\n"
		"\t[-l squelch_level (default: 0/off)]\n"
		//"\t    for fm squelch is inverted\n"
		//"\t[-o oversampling (default: 1, 4 recommended)]\n"
		"\t[-p ppm_error (default: 0)]\n"
		"\t[-E enable_option (default: none)]\n"
		"\t    use multiple -E to enable multiple options\n"
		"\t    edge:   enable lower edge tuning\n"
		"\t    dc:     enable dc blocking filter\n"
		"\t    deemp:  enable de-emphasis filter\n"
		"\t    direct: enable direct sampling\n"
		"\t    offset: enable offset tuning\n"
		"\tfilename ('-' means stdout)\n"
		"\t    omitting the filename also uses stdout\n\n"
		"Experimental options:\n"
		"\t[-r resample_rate (default: none / same as -s)]\n"
		"\t[-t squelch_delay (default: 10)]\n"
		"\t    +values will mute/scan, -values will exit\n"
		"\t[-F fir_size (default: off)]\n"
		"\t    enables low-leakage downsample filter\n"
		"\t    size can be 0 or 9.  0 has bad roll off\n"
		"\t[-A std/fast/lut choose atan math (default: std)]\n"
		//"\t[-C clip_path (default: off)\n"
		//"\t (create time stamped raw clips, requires squelch)\n"
		//"\t (path must have '\%s' and will expand to date_time_freq)\n"
		//"\t[-H hop_fifo (default: off)\n"
		//"\t (fifo will contain the active frequency)\n"
		"\n"
		"Produces signed 16 bit ints, use Sox or aplay to hear them.\n"
		"\trtl_fm ... | play -t raw -r 24k -es -b 16 -c 1 -V1 -\n"
		"\t           | aplay -r 24k -f S16_LE -t raw -c 1\n"
		"\t  -M wbfm  | play -r 32k ... \n"
		"\t  -s 22050 | multimon -t raw /dev/stdin\n\n");
	exit(1);
}

void frequency_range(struct controller_state *s, char *arg)
{
	char *start, *stop, *step;
	int i;
	start = arg;
	stop = strchr(start, ':') + 1;
	stop[-1] = '\0';
	step = strchr(stop, ':') + 1;
	step[-1] = '\0';
	for (i = (int) atofs(start); i <= (int) atofs(stop); i += (int) atofs(step))
	{
		s->freqs[s->freq_len] = (uint32_t) i;
		s->freq_len++;
		if (s->freq_len >= FREQUENCIES_LIMIT)
		{
			break;
		}
	}
	stop[-1] = ':';
	step[-1] = ':';
}

void sanity_checks(void)
{
	if (controller.freq_len == 0)
	{
		fprintf(stderr, "Please specify a frequency.\n");
		exit(1);
	}

	if (controller.freq_len >= FREQUENCIES_LIMIT)
	{
		fprintf(stderr, "Too many channels, maximum %i.\n", FREQUENCIES_LIMIT);
		exit(1);
	}

	if (controller.freq_len > 1 && demod.squelch_level == 0)
	{
		fprintf(stderr, "Please specify a squelch level.  Required for scanning multiple frequencies.\n");
		exit(1);
	}

}

void parse_interactive(struct interact_state *i, char * start)
{
	int parsed[3] = { MIN_FREQ_DEFAULT, MAX_FREQ_DEFAULT, BANDWIDTH_EU };
	//make sure they are zero
	memset(parsed, 0, sizeof(int) * 3);
	char * temp;
	char * data = start;
	int x;
	for (x = 0; x < 3; x++)
	{
		temp = strchr(data, ':');
		if (temp != NULL)
		{
			temp += 1;
			temp[-1] = '\0';
			parsed[x] = atofs(data);
			data = temp;
		}
		else
		{
			return;
		}
	}
	interact_setup(parsed[0], parsed[1], parsed[2]);
}

#pragma endregion Private Main Helpers

#ifdef _Integrated_Into_Other
int rtl_fm_init(int argc, char ** argv)
{
#else
int main(int argc, char **argv)
{
#endif // _Integrated_Into_Other

#pragma region
	struct sigaction sigact;

	int r, opt;
	bool interactive = false;
	bool dev_given = false;
	int custom_ppm = 0;
	bool enable_biastee = false;

	dongle_init(&dongle);
	demod_init(&demod);
	output_init(&output);
	controller_init(&controller);
	interact_init(&interact);
	player_init(&player);
#pragma endregion Declarations and initialization
#pragma region
	//TODO: add opt for port to listen on
	while ((opt = getopt(argc, argv, "d:f:g:s:b:i:l:o:t:r:p:c:i:E:F:A:M:h:T")) != -1)
	{
		switch (opt)
		{

//TODO: parse opt
		case 'd':
		dongle.dev_index = verbose_device_search(optarg);
		dev_given = true;
		break;
		case 'f':
		if (controller.freq_len >= FREQUENCIES_LIMIT)
		{
			break;
		}
		if (strchr(optarg, ':'))
		{
			frequency_range(&controller, optarg);
		}
		else
		{
			controller.freqs[controller.freq_len] = (uint32_t) atofs(optarg);
			controller.freq_len++;
		}
		break;
		case 'g':
		dongle.gain = (int) (atof(optarg) * 10);
		break;
		case 'l':
		demod.squelch_level = (int) atof(optarg);
		//if squelch is not 0, then we mute, as per specification
		//note that this does not apply if there are multiple frequencies
		//it will be cleared (set to false) if, after parsing, there are multiple frequencies
		dongle.muteAll = demod.squelch_level != 0;
		break;
		case 's':
		demod.rate_in = (uint32_t) atofs(optarg);
		demod.rate_out = (uint32_t) atofs(optarg);
		break;
		case 'r':
		output.rate = (int) atofs(optarg);
		demod.rate_out2 = (int) atofs(optarg);
		break;
		case 'o':
		fprintf(stderr, "Warning: -o is very buggy\n");
		demod.post_downsample = (int) atof(optarg);
		if (demod.post_downsample < 1 || demod.post_downsample > MAXIMUM_OVERSAMPLE)
		{
			fprintf(stderr, "Oversample must be between 1 and %i\n", MAXIMUM_OVERSAMPLE);
		}
		break;
		case 't':
		demod.conseq_squelch = (int) atof(optarg);
		if (demod.conseq_squelch < 0)
		{
			demod.conseq_squelch = -demod.conseq_squelch;
			fprintf(stderr, "Terminating once squelch count hits %i\n", demod.conseq_squelch);
			demod.terminate_on_squelch = true;
		}
		break;
		case 'p':
		dongle.ppm_error = atoi(optarg);
		custom_ppm = 1;
		break;
		case 'E':
		if (strcmp("edge", optarg) == 0)
		{
			controller.edge = 1;
		}
		if (strcmp("dc", optarg) == 0)
		{
			demod.dc_block = true;
		}
		if (strcmp("deemp", optarg) == 0)
		{
			demod.deemph = true;
		}
		if (strcmp("direct", optarg) == 0)
		{
			dongle.direct_sampling = true;
		}
		if (strcmp("offset", optarg) == 0)
		{
			dongle.offset_tuning = true;
		}
		break;
		case 'F':
		demod.downsample_passes = 1;  /* truthy placeholder */
		demod.comp_fir_size = atoi(optarg);
		break;
		case 'A':
		if (strcmp("std", optarg) == 0)
		{
			demod.custom_atan = 0;
		}
		if (strcmp("fast", optarg) == 0)
		{
			demod.custom_atan = 1;
		}
		if (strcmp("lut", optarg) == 0)
		{
			atan_lut_init();
			demod.custom_atan = 2;
		}
		break;
		case 'M':
		if (strcmp("fm", optarg) == 0)
		{
			demod.mode_demod = &fm_demod;
		}
		if (strcmp("raw", optarg) == 0)
		{
			demod.mode_demod = &raw_demod;
		}
		if (strcmp("am", optarg) == 0)
		{
			demod.mode_demod = &am_demod;
		}
		if (strcmp("usb", optarg) == 0)
		{
			demod.mode_demod = &usb_demod;
		}
		if (strcmp("lsb", optarg) == 0)
		{
			demod.mode_demod = &lsb_demod;
		}
		if (strcmp("wbfm", optarg) == 0)
		{
			controller.wb_mode = true;
			demod.mode_demod = &fm_demod;
			demod.rate_in = 170000;
			demod.rate_out = 170000;
			demod.rate_out2 = 32000;
			demod.custom_atan = 1;
			//demod.post_downsample = 4;
			demod.deemph = true;
			demod.squelch_level = 0;
		}
		break;
		case 'T':
		enable_biastee = true;
		break;
		case 'i':
		interactive = true;
		if (strlen(optarg) != 0)
		{
			parse_interactive(&interact, optarg);
		}
		case 'h':
		default:
		usage();
		break;
		}
	}

	/* quadruple sample_rate to limit to Δθ to ±π/2 */
	demod.rate_in *= demod.post_downsample;

	if (output.rate == 0)
	{
		output.rate = demod.rate_out;
	}

	//ADDED: makes sure the rate is right.
	player.rate = output.rate;
	//END ADDED
	sanity_checks();

	//ADDED: sets up player so it can play things.
	player_setup(&player);
	//END ADDED

	if (controller.freq_len > 1)
	{
		//clear mute flag, as squelch means we have multiple frequencies
		dongle.muteAll = false;
		controller.status = SEEKING;
		interact.status = SEEKING;
	}
	else if (interactive)
	{
		dongle.muteAll = false;
		if (demod.squelch_level == 0)
		{
			demod.squelch_level = SQUELCH_DEFAULT;
		}
	}

	ACTUAL_BUF_LENGTH = lcm_post[demod.post_downsample] * DEFAULT_BUF_LENGTH;

	if (!dev_given)
	{
		dongle.dev_index = verbose_device_search("0");
	}

	if (dongle.dev_index < 0)
	{
		exit(1);
	}

	r = rtlsdr_open(&dongle.dev, (uint32_t) dongle.dev_index);
	if (r < 0)
	{
		fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dongle.dev_index);
		exit(1);
	}

	sigact.sa_handler = sighandler;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	sigaction(SIGQUIT, &sigact, NULL);
	sigaction(SIGPIPE, &sigact, NULL);

	if (demod.deemph)
	{
		demod.deemph_a = (int) round(1.0 / ((1.0 - exp(-1.0 / (demod.rate_out * 75e-6)))));
	}

	/* Set the tuner gain */
	if (dongle.gain == AUTO_GAIN)
	{
		verbose_auto_gain(dongle.dev);
	}
	else
	{
		dongle.gain = nearest_gain(dongle.dev, dongle.gain);
		verbose_gain_set(dongle.dev, dongle.gain);
	}

	//rtlsdr uses c90 bool of an int. so to prevent potential conflicts, a typecast.
	rtlsdr_set_bias_tee(dongle.dev, enable_biastee ? 1 : 0);
	if (enable_biastee)
	{
		fprintf(stderr, "activated bias-T on GPIO PIN 0\n");
	}
	verbose_ppm_set(dongle.dev, dongle.ppm_error);

	//r = rtlsdr_set_testmode(dongle.dev, 1);

	/* Reset endpoint before we start reading from it (mandatory) */
	verbose_reset_buffer(dongle.dev);
#pragma endregion Setup
#pragma region 
	pthread_create(&controller.thread, NULL, controller_thread_fn, (void *) (&controller));
	usleep(100000);
	pthread_create(&output.thread, NULL, output_thread_fn, (void *) (&output));
	pthread_create(&demod.thread, NULL, demod_thread_fn, (void *) (&demod));
	pthread_create(&dongle.thread, NULL, dongle_thread_fn, (void *) (&dongle));
	pthread_create(&player.thread, NULL, player_thread_fn, (void *) (&player));
	//TODO: create a thread to parse commands

#ifdef _Integrated_Into_Other
	return r;
}
int rtl_fm_exit(int r)
{
#endif 
#pragma endregion thread creation
	while (!do_exit)
	{
		usleep(100000);
	}

	if (demod.squelch_exit)
	{
		fprintf(stderr, "\nSquelch Count Reached, exiting...\n");
	}
	else if (do_exit)
	{
		fprintf(stderr, "\nUser cancel, exiting...\n");
	}
	else
	{
		fprintf(stderr, "\nLibrary error %d, exiting...\n", r);
	}
#pragma region
	//cancel the callback
	if (player.pcm_callback != NULL)
	{
		snd_async_del_handler(player.pcm_callback);
		player.pcm_callback = NULL;
	}
	rtlsdr_cancel_async(dongle.dev);

	pthread_join(player.thread, NULL);
	pthread_join(dongle.thread, NULL);
	safe_cond_signal(&demod.ready, &demod.ready_m);
	pthread_join(demod.thread, NULL);
	safe_cond_signal(&output.ready, &output.ready_m);
	pthread_join(output.thread, NULL);
	safe_cond_signal(&controller.interact, &controller.interact_m);
	pthread_join(controller.thread, NULL);

	//dongle has no locks and nothing on the heap (no mallocs), so nothing to clean up.
	demod_cleanup(&demod);
	output_cleanup(&output);
	controller_cleanup(&controller);
	interact_cleanup(&interact);
	player_cleanup(&player);

	//drop any remaining frames and close.
	snd_pcm_drop(player.pcm_handle);
	snd_pcm_close(player.pcm_handle);

	rtlsdr_close(dongle.dev);
#pragma endregion cleanup
	return r >= 0 ? r : -r;
}

// vim: tabstop=8:softtabstop=8:shiftwidth=8:noexpandtab
